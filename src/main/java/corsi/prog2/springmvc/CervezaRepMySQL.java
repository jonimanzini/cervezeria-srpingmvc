package corsi.prog2.springmvc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository // Si la calse es de persistencia, debe anotarse como @Repository
public class CervezaRepMySQL implements CervezasRepository {

    private final String dbFullURL;
    private final String dbUser;
    private final String dbPswd;

    @Autowired  // Inyección en el contructor (constructor injection)
    public CervezaRepMySQL(
            @Qualifier("dbName") String dbName,
            @Qualifier("dbURL")  String dbURL,
            @Qualifier("dbUser") String dbUser,
            @Qualifier("dbPswd") String dbPswd) {
        dbFullURL = "jdbc:mysql://" + dbURL + "/" + dbName;
        this.dbUser = dbUser;
        this.dbPswd = dbPswd;
    }

    @Override
    public List<Cerveza> getCervezasByColor(String color) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from Cerveza where color = '" + color + "'");
        
        List<Cerveza> c = q.getResultList();
        s.getTransaction().commit();
        s.close();
        
        return c;
    }

    @Override
    public List<String> getColores() {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        Query q = s.createQuery("from Cerveza");
        
        List<Cerveza> c = q.getResultList();
        s.getTransaction().commit();
        s.close();
        
        HashSet<String> resultado = new HashSet<>();
       
        for(Cerveza x : c)
        {
            resultado.add(x.getColor());
        }
        
        ArrayList<String> array= new ArrayList<>();
        array.addAll(resultado);
        
        return  /*(List<String>)*/ array;
    }

}
